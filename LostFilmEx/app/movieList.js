﻿function MovieList() {
    var items;
    this.get = function (invalidateCache) {
        if (items && !invalidateCache) return items;

        items = new Array();

        var item = new MovieItem();
        var elementIndex = 1;

        $("#new_sd_list").children().each(function (index) {
            var child = $(this);
            if (child.hasClass("a_download")) {
                items.push(item);
                item = new MovieItem();
                elementIndex = 0;
            }

            console.log(child.prop("tagName") + elementIndex + " " + child.text().trim());

            if (elementIndex === 1 && child.prop("tagName") === "DIV") {
                item.setDate(child.text().trim());
            } else if (elementIndex === 2 && child.prop("tagName") === "SPAN") {
                item.setName(child.text().trim());
            } else if (elementIndex === 3 && child.prop("tagName") === "DIV") {
                var picElement = child.find("img.category_icon").get(0);
                if (picElement) {
                    item.setPictureUrl($(picElement).attr("src"));
                }
                var titleElement = child.find(".torrent_title").get(0);
                if (titleElement) {
                    item.setSeriesTitle($(titleElement).text().trim());
                }
            }

            if (child.prop("tagName") !== "BR")
                elementIndex++;
        });

        return items;

    };
};

function MovieItem(nameValue, seriesTitleValue, pictureUrlValue, dateValue) {
    var name = nameValue;

    this.getName = function () {
        return name;
    };

    this.setName = function (value) {
        name = value;
    };

    var seriesTitle = seriesTitleValue;

    this.getSeriesTitle = function () {
        return seriesTitle;
    };

    this.setSeriesTitle = function (value) {
        seriesTitle = value;
    };

    var pictureUrl = pictureUrlValue;

    this.getPictureUrl = function () {
        return pictureUrl;
    };

    this.setPictureUrl = function (value) {
        pictureUrl = value;
    };

    var date = dateValue;

    this.getDate = function () {
        return date;
    };

    this.setDate = function (value) {
        date = value;
    };

};

chrome.runtime.connect().postMessage((new MovieList()).get());
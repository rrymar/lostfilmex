﻿chrome.runtime.onConnect.addListener(function (port) {
    var tab = port.sender.tab;

    // This will get called by the content script we execute in
    // the tab as a result of the user pressing the browser action.
    port.onMessage.addListener(function (message) {
        console.log(message);
    });
});

chrome.browserAction.onClicked.addListener(function (tab) {
    chrome.tabs.executeScript(null, { file: "movieList.js" });
});


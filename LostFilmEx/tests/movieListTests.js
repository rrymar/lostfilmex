﻿/// <reference path="lostFilmHtml.js" />
/// <reference path="lib/jasmine.js" />
/// <reference path="lib/jquery-2.1.4.min.js" />
/// <reference path="~/app/movieList.js" />

describe("LostFilm movieList tests", function () {

    var movieList;
    beforeEach(function () {
        if (movieList) return;

        var data = $(html);
        $(document.body).append(data);
        movieList = new MovieList();
    });

    it("movie list finded", function () {
        var movies = movieList.get();
        expect(movies.length).toBe(5);

    });

    describe("- movieItem tests ", function () {
        var expected0 = new MovieItem("Джессика Джонс", "Дамский вечер (Ladies Night)",
            "/Static/icons/cat_jessica_jones.jpeg", "01.01");
        it("movie #0", function () {
            validateMovieItem(movieList.get()[0], expected0);
        });

        var expected1 = new MovieItem("Области тьмы", "Рукотворный Армагеддон (Arm-aggedon)",
            "/Static/icons/cat_limitless.jpeg", "01.10");
        it("movie #1", function () {
            validateMovieItem(movieList.get()[1], expected1);
        });

        var expected4 = new MovieItem("Слепое пятно", "Самодельное орудие зла (Evil Handmade Instrument)",
            "/Static/icons/cat_blindspot.jpeg", "01.10");
        it("movie #4", function () {
            validateMovieItem(movieList.get()[4], expected4);
        });

    });

});

function validateMovieItem(actual, expected) {
    expect(actual != null).toBe(true);
    expect(actual.getName()).toBe(expected.getName());
    expect(actual.getSeriesTitle()).toBe(expected.getSeriesTitle());
    expect(actual.getPictureUrl()).toBe(expected.getPictureUrl());
    expect(actual.getDate()).toBe(expected.getDate());
}